<?php
//Session
$session = $this->session->get_userdata(); 
$session_name = $session['name'];
$session_id = $session['id'];
$session_role = $session['role'];
$allow_supply = $session['allow_supply'];
$role_admin = 1;
$is_admin = $session_role == $role_admin;
$session_role_name = $is_admin? 'Administrator': 'Pengguna';

//Flash message thing
$success = $this->session->flashdata('success');
$warning = $this->session->flashdata('warning');
if(isset($success) || isset($warning)) {
	$flag = isset($success)?'success':'warning';
	$message = isset($success)? $success:$warning;
}
$has_message = isset($message);
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Warehousing</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<script src="<?php echo base_url()?>assets/js/libs/jquery/jquery-1.11.2.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/libs/bootstrap/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/libs/spin.js/spin.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/libs/autosize/jquery.autosize.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>

		<!-- BEGIN STYLESHEETS -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-default/bootstrap.css?1422792965" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-default/materialadmin.css?1425466319" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-default/font-awesome.min.css?1422529194" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-default/material-design-iconic-font.min.css?1421434286" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>assets/css//theme-default/libs/bootstrap-datepicker/datepicker3.css">
		<?php
		if(isset($list)) {?>
			<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-default/libs/DataTables/jquery.dataTables.css">
		<?php
		}?>
		
		<!-- END STYLESHEETS -->

		<!-- ADITIONAL CSS FOR TOAST -->
		<?php
		if ($has_message) {?>
			<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-default/libs/toastr/toastr.css">
		<?php
		} ?>
			
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/libs/utils/html5shiv.js?1403934957"></script>
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/libs/utils/respond.min.js?1403934956"></script>
		<![endif]-->
	</head>
	<body class="menubar-hoverable header-fixed menubar-first full-content  menubar-pin">

		<!-- BEGIN HEADER-->
		<header id="header" >
			<div class="headerbar">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="headerbar-left">
					<ul class="header-nav haaeader-nav-options">
						<li class="header-nav-brand" >
							<div class="brand-holder">
								<a href="dashboard">
									<span class="text-lg text-bold text-primary">Warehousing</span>
								</a>
							</div>
						</li>
						<li>
							<a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
								<i class="fa fa-bars"></i>
							</a>
						</li>
					</ul>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="headerbar-right">
					<!--TODO show this header-nav-options-->
					<ul class="header-nav header-nav-profile">
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
								<img src="<?php echo base_url()?>assets/img/avatar1.jpg" alt="" />
								<span class="profile-info">
									<?=$session_name?>
									<small><?=$session_role_name?></small>
								</span>
							</a>
							<ul class="dropdown-menu animation-dock">
								<li class="dropdown-header">Pengaturan</li>
								<li><a href="profil">Profil</a></li>
								<li class="divider"></li>
								<!--<li><a href="html/pages/locked.html"><i class="fa fa-fw fa-lock"></i> Lock</a></li>-->
								<li><a href="<?=base_url('logout')?>"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
							</ul><!--end .dropdown-menu -->
						</li><!--end .dropdown -->
					</ul><!--end .header-nav-profile -->
					<ul class="header-nav header-nav-toggle hidden">
						<li>
							<a class="btn btn-icon-toggle btn-default" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
								<i class="fa fa-ellipsis-v"></i>
							</a>
						</li>
					</ul><!--end .header-nav-toggle -->
				</div><!--end #header-navbar-collapse -->
			</div>
		</header>
		<!-- END HEADER-->

		<!-- BEGIN BASE-->
		<div id="base">

			<!-- BEGIN CONTENT-->
			<div id="content">
			
			<?php echo $_content; ?>

			</div><!--end #content-->
			<!-- END CONTENT -->

			<!-- BEGIN MENUBAR-->
			<div id="menubar" class="menubar-inverse ">
				<div class="menubar-fixed-panel">
					<div>
						<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
							<i class="fa fa-bars"></i>
						</a>
					</div>
					<div class="expanded">
						<a href="dashboard">
							<span class="text-lg text-bold text-primary ">Warehousing</span>
						</a>
					</div>
				</div>
				<div class="menubar-scroll-panel">

					<!-- BEGIN MAIN MENU -->
					<ul id="main-menu" class="gui-controls">

						<!-- BEGIN DASHBOARD -->
						<li <?php if($nav=='dashboard') echo'class="active"';?>>
							<a href="<?php echo base_url('dashboard');?>">
								<div class="gui-icon"><i class="md md-home"></i></div>
								<span class="title">Beranda</span>
							</a>
						</li><!--end /menu-li -->
						<!-- END DASHBOARD -->
						<!-- BEGIN SISWA -->
						<?php
						if ($is_admin) {?>
						<li <?php if($nav=='category') echo'class="active"';?>>
							<a href="<?php echo base_url('category');?>" >
								<div class="gui-icon"><i class="md md-bookmark"></i></div>
								<span class="title">Kategori Barang</span>
							</a>
						</li>
						<?php
						}?>

						<li <?php if($nav=='item') echo'class="active"';?>>
							<a href="<?php echo base_url('item');?>" >
								<div class="gui-icon"><i class="md md-dns"></i></div>
								<span class="title"> Barang</span>
							</a>
						</li>

						<?php
						if ($is_admin) {?>
							<li <?php if($nav=='user') echo'class="active"';?>>
								<a href="<?php echo base_url('user');?>" >
									<div class="gui-icon"><i class="md md-person"></i></div>
									<span class="title"> Pengguna</span>
								</a>
							</li>
						<?php
						}?>

						<?php
						if ($is_admin || $allow_supply) {?>
						<li <?php if($nav=='supply') echo'class="active"';?>>
							<a href="<?php echo base_url('supply');?>" >
								<div class="gui-icon"><i class="md md-inbox"></i></div>
								<span class="title"> Stok Barang</span>
							</a>
						</li>
						<?php
						}?>

						<li <?php if($nav=='order') echo'class="active"';?>>
							<a href="<?php echo base_url('order');?>" >
								<div class="gui-icon"><i class="md md-shopping-cart"></i></div>
								<span class="title"> Pemesanan</span>
							</a>
						</li>
					</ul><!--end .main-menu -->
					<!-- END MAIN MENU -->

					<div class="menubar-foot-panel">
						<small class="no-linebreak hidden-folded">
							<span class="opacity-75">Copyright &copy; 2014</span> <strong>CodeCovers</strong>
						</small>
					</div>
				</div><!--end .menubar-scroll-panel-->
			</div><!--end #menubar-->
			<!-- END MENUBAR -->

		</div><!--end #base-->
		<!-- END BASE -->
		<!-- BEGIN JAVASCRIPT -->
		<script src="<?php echo base_url();?>assets/js/core/source/App.js"></script>
		<script src="<?php echo base_url();?>assets/js/core/source/AppNavigation.js"></script>
		<script src="<?php echo base_url();?>assets/js/core/source/AppOffcanvas.js"></script>
		<script src="<?php echo base_url();?>assets/js/core/source/AppCard.js"></script>
		<script src="<?php echo base_url();?>assets/js/core/source/AppForm.js"></script>
		<script src="<?php echo base_url();?>assets/js/core/source/AppNavSearch.js"></script>
		<script src="<?php echo base_url();?>assets/js/core/source/AppVendor.js"></script>
		<!-- END JAVASCRIPT -->
		
		<!--If using data table-->
		<?php
		if(isset($list)) {?>	
			<script src="<?php echo base_url();?>assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
			<script>
				$('#table').DataTable({ 
					"dom": 'lCfrtip',
					"order": [],
					"colVis": {
						"buttonText": "Columns",
						"overlayFade": 0,
						"align": "right"
					},
					"language": {
						"lengthMenu": '_MENU_ data per halaman',
						"zeroRecords": 'Hasil pencarian tidak ditemukan',
  						"infoEmpty":      "Tidak ada yang ditampilkan",
			  	  		"infoFiltered":   "(difilter dari _MAX_ data)",
						"search": '<i class="fa fa-search"></i>',
						"paginate": {
							"previous": '<i class="fa fa-angle-left"></i>',
							"next": '<i class="fa fa-angle-right"></i>'
						},
						"info":           "_START_ - _END_ dari _TOTAL_ data",
					}
				});
			</script>
		<?php
		}?>
		
		<!-- Show success toast if validation defined-->
		<?php 
		if($has_message) {
			?>
			<script src="<?php echo base_url();?>assets/js/libs/toastr/toastr.js"></script>
			<script src="<?php echo base_url();?>assets/js/core/demo/DemoUIMessages.js"></script>
			<script>
				toastr.options.hideDuration = 0;
				toastr.clear();
				
				toastr.options.closeButton = false;
				toastr.options.progressBar = false;
				toastr.options.debug = false;
				toastr.options.positionClass = 'toast-bottom-left';
				toastr.options.showEasing = 'swing';
				toastr.options.hideEasing = 'swing';
				toastr.options.showMethod = 'slideDown';
				toastr.options.hideMethod = 'slideUp';
				
				toastr.options.timeOut = 3000;
				toastr.options.positionClass = 'toast-bottom-left';
				<?php
				echo"
				toastr.$flag('$message', '');
				";?>
			</script>
		<?php 
		} ?>
	</body>
</html>
