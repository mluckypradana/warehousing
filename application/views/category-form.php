<section style="min-height: 480px">
	<div class="section-header">
		<h2 class="text-primary"><?php echo $label_title;?></h2>
	</div>
	<div class="section-body ">
		<!--Subtitle-->
		<div class="row">
			<div class="col-md-8">
				<article class="margin-bottom-xxl">
					<p class="lead">
						<?php echo$label_subtitle;?></p>
				</article>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-body">
						<form class="form" role="form" method="POST" 
								action="<?php echo$form_action;?>">
							<input type="hidden" name="id" value="<?php echo $data['id'];?>"/>
							<?php
							//Alert and validation
							$validation = validation_errors();
							if($validation!=''){
								echo'
								<div class="col-lg-12">
									<div class="form-group alert alert-warning" role="alert">
										'.$validation.'
									</div>
								</div>';
							}
							?>

							<div class="col-md-12 col-sm-12">
								<!--Textfield-->
								<?php $field = 'name'; ?>
								<div class="form-group floating-label">
									<input name="<?=$field?>" type="text" class="form-control" id="<?=$field?>"
									value="<?php echo set_value($field, $data[$field]); ?>" >
									<label for="<?=$field?>">Nama</label>
								</div>
							
								<div class="card-actionbar-row">
									<a href="<?php echo base_url($nav); ?>" type="submit" class="btn btn-raised btn-default ink-reaction">
											<?php echo 'Batal';?></a>&nbsp;&nbsp;&nbsp;
									<button type="submit" class="btn btn-raised btn-primary ink-reaction">
											<?php echo $label_submit;?></button>
								</div>
							</div><!--end .col -->
						
						</form>
					</div><!--end .card-body -->
				</div><!--end .card -->
			</div>
			
		</div>
	</div>
</section>
<script src="<?php echo base_url();?>assets/js/libs/moment/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script>$('#datepicker').datepicker({autoclose: true, todayHighlight: true});</script>
