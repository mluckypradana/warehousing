<!-- BEGIN NODATA MESSAGE -->
<section>
	<div class="section-body contain-lg">
		<div class="row">
			<div class="col-lg-12 text-center">
				
				<h1>
				<br/><br/>
				<span class="text-xxxl text-light"><i class="fa md-search text-default-light"></i></span></h1>
				<h2 class="text-light">Tidak ada data pada menu ini</h2></h2>
				<br/>
				<a href="<?php echo $add_link;?>"><button type="button" class="btn ink-reaction btn-raised btn-primary">Tambahkan data baru</button></a>
			</div><!--end .col -->
		</div><!--end .row -->
	</div><!--end .section-body -->	
</section>