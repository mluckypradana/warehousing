<!-- BEGIN 404 MESSAGE -->
<section>
	<div class="section-body">
		<div class="row">
			<div class="col-md-4">
				<div class="card">
					<div class="card-body no-padding">
						<div class="alert alert-callout alert-success no-margin">
							<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>
							<strong class="text-xl"><?=$category_total?> kategori barang</strong><br>
							<span class="opacity-50">&nbsp;</span>
						</div>
					</div><!--end .card-body -->
				</div><!--end .card -->
			</div>

			<div class="col-md-4">
				<div class="card">
					<div class="card-body no-padding">
						<div class="alert alert-callout alert-success no-margin">
							<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>
							<strong class="text-xl"><?=$item_total?> barang</strong><br>
							<span class="opacity-50">&nbsp;</span>
						</div>
					</div><!--end .card-body -->
				</div><!--end .card -->
			</div>


			<div class="col-md-4">
				<div class="card">
					<div class="card-body no-padding">
						<div class="alert alert-callout alert-success no-margin">
							<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>
							<strong class="text-xl"><?=$user_total?> pengguna</strong><br>
							<span class="opacity-50">&nbsp;</span>
						</div>
					</div><!--end .card-body -->
				</div><!--end .card -->
			</div>
		</div>
		<div class="row">

			<?php
			if($need_change_password) {
			?>
				<!-- Change password layout -->
				<div class="col-lg-4">
					<div class="card">
						<div class="card-head">
							<header class="text-primary">Ubah password akun baru
							</header>
						</div>
						<div class="card-body" style="margin-top: -32px;">
							<form class="form" role="form" method="POST" 
									action="<?=base_url('user/change_password')?>">
								<input type="hidden" name="id" value="<?php echo $session_id;?>"/>
								<?php
								//Alert and validation
								$validation = validation_errors();
								if($validation!=''){
									echo'
									<div class="col-lg-12">
										<div class="form-group alert alert-warning" role="alert">
											'.$validation.'
										</div>
									</div>';
								}
								?>
								<div class="col-md-12 col-sm-12">
									<!--Textfield-->
									<?php $field = 'password'; ?>
									<div class="form-group floating-label">
										<input name="<?=$field?>" type="password" class="form-control" id="<?=$field?>"
										value="<?php echo set_value($field, $data[$field]); ?>" >
										<label for="<?=$field?>">Password</label>
									</div>

									<!--Textfield-->
									<?php $field = 'confirm_password'; ?>
									<div class="form-group floating-label">
										<input name="<?=$field?>" type="password" class="form-control" id="<?=$field?>"
										value="<?php echo set_value($field, $data[$field]); ?>" >
										<label for="<?=$field?>">Konfirmasi Password</label>
									</div>
								<a href="<?php echo base_url($nav); ?>" type="submit" class="btn btn-raised btn-default ink-reaction">
												<?php echo 'Batal';?></a>&nbsp;&nbsp;&nbsp;
										<button type="submit" class="btn btn-raised btn-primary ink-reaction">
												Ubah</button>
								</div><!--end .col -->
							
							</form>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div>
			<?php
			}
			else {?>
				
			<?php
			}?>
		</div><!--end .row -->
	</div><!--end .section-body -->	
</section>