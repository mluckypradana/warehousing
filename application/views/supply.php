<?php
$edit_enabled = false;
?>
<section class="style-default-bright" style="min-height: 600px">	
	<div class="section-header">
		<div class="row">
		<div class="col-lg-8">
		<h2 class="text-primary"><?=ucfirst($object_label)?></h2>
		</div>
		<div class="col-lg-4" style="text-align: right">
		<a href="<?php echo $add_link;?>"><button type="button" class="btn ink-reaction btn-floating-action btn-primary"><i class="md md-add"></i></button></a>
		</div>
		</div>
	</div>
	<!--Section body-->
	<div class="section-body">									
		
		<!--Table-->
		<div class="row">
			<div class="col-lg-12">
				<div class="table-responsive">
					<table id="table" class="table table-striped table-hover dataTable no-footer">
					<thead>
						<tr role="row">
							<th class="sorting" aria-controls="table-hovere" style="width:150px">Tanggal masuk</th>
							<th class="sorting" aria-controls="table" style="width:250px">Penyuplai</th>
							<th class="sorting" aria-controls="table">Nama Barang</th>
							<th class="sorting" aria-controls="table" style="width:160px">Stok</th>
							<th style="width: 40px;">Aksi</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach ($list as $row){
						echo'
						<tr class="gradeX odd" role="row">
							<td>'.$row['created_date'].'</td>
							<td>'.$row['user_name'].'</td>
							<td>'.$row['item_name'].'</td>
							<td>'.$row['total'].' '.$row['unit_name'].'</td>
							<td>';

						if($edit_enabled)
							echo'
								<a href="'.base_url($nav).'/edit/'.$row['id'].'" class="btn btn-icon-toggle btn-default" 
										style="margin-top: -8px;margin-bottom: -8px;">
										<i class="md md-edit"></i></a>';
						echo'
								<a href="#" class="btn btn-icon-toggle btn-default" 
										style="margin-top: -8px;margin-bottom: -8px;"
										onClick="document.getElementById(\'btn-delete\').href = \''.base_url($nav.'/delete/'.$row['id']).'\';">
										<i class="md md-delete" data-toggle="modal" data-target="#simpleModal"></i></a>
								</td>
						</tr>';
						
					}
					?>
						
					</tbody>
				</table>
				</div><!--end .table-responsive -->
			</div>
		</div>
	</div>
</section>

<!-- BEGIN SIMPLE MODAL MARKUP -->
<div class="modal fade" id="simpleModal" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="simpleModalLabel">Yakin akan membatalkan suplai</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a href="#" data-method="DELETE" id="btn-delete"><button type="button" class="btn btn-danger">Hapus</button></a>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END SIMPLE MODAL MARKUP -->