<?php
//Session
$session = $this->session->get_userdata(); 
$session_name = $session['name'];
$session_id = $session['id'];
$session_role = $session['role'];
$role_admin = 1;
$is_admin = $session_role == $role_admin;
$session_role_name = $is_admin? 'Administrator': 'Pengguna';

$edit_enabled = false;
?>
<section class="style-default-bright" style="min-height: 600px">	
	<div class="section-header">
		<div class="row">
		<div class="col-lg-8">
		<h2 class="text-primary"><?=ucfirst($object_label)?></h2>
		</div>
		<div class="col-lg-4" style="text-align: right">
		<?php
		if(!$is_admin){?>
		<a href="<?php echo $add_link;?>"><button type="button" class="btn ink-reaction btn-floating-action btn-primary"><i class="md md-add"></i></button></a>
		<?php
		}?>

		</div>
		</div>
	</div>
	<!--Section body-->
	<div class="section-body">									
		
		<div class="row">

			<!-- BEGIN INBOX NAV -->
			<div class="col-sm-4 col-md-3 col-lg-2">
				<!-- <form class="navbar-search margin-bottom-xxl" role="search">
					<div class="form-group">
						<input type="text" class="form-control" name="contactSearch" placeholder="Enter your keyword">
					</div>
					<button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
				</form> -->
				<ul class="nav nav-pills nav-stacked nav-icon">
					<li><small>KATEGORI</small></li>
					<li<?php if($subnav=='all'||$subnav==''){echo' class="active"';}?>><a href="<?=base_url('order/all')?>"><span class="md md-border-all"></span>Semua <small>(<?=$all_total?>)</small></a></li>
					<li<?php if($subnav=='approved'){echo' class="active"';}?>><a href="<?=base_url('order/approved')?>"><span class="md md-check-box"></span>Disetujui <small>(<?=$approved_total?>)</small></a></li>
					<li<?php if($subnav=='unapproved'){echo' class="active"';}?>><a href="<?=base_url('order/unapproved')?>"><span class="md md-check-box-outline-blank"></span>Belum disetujui <small>(<?=$unapproved_total?>)</small></a></li>
					<!-- <li><small>Tags</small></li>
					<li><a href="#"><i class="fa fa-dot-circle-o text-info"></i>Unread</a></li>
					<li><a href="#"><i class="fa fa-dot-circle-o text-danger"></i>Important</a></li>
					<li><a href="#"><i class="fa fa-dot-circle-o text-success"></i>Success</a></li> -->
				</ul>
			</div><!--end . -->
			<!-- END INBOX NAV -->

			<div class="col-sm-8 col-md-9 col-lg-10">
				<div class="text-divider visible-xs"><span>Email list</span></div>
				<div class="row">

					<!-- BEGIN INBOX EMAIL LIST -->
					<div class="nano has-scrollbar" style="height: 240px;"><div class="nano-content" tabindex="0" style="right: -17px;">
					<div class="col-md-12 height-6 scroll-sm" style="height: auto;">
						<div class="list-group list-email list-gray">
							<?php
							foreach ($list as $row){
								$icon = $row['status']? 'md-check-box':'md-check-box-outline-blank';
								$description = $row['status']? 
									'Pemesanan dengan nomor order '.$row['order_number'].' sudah disetujui oleh '.$row['approved_user_name']:
										'Pemesanan dengan nomor order '.$row['order_number'].' belum disetujui oleh pihak administrator';
								?>
								<a class="list-group-item">
									<h5><?=$row['user_name']?></h5>
									<h4>
									Pemesanan <?=$row['item_name']?> sebanyak <?=$row['total']?> <?=$row['unit_name']?>
									</h4>
									<p class="hidden-xs hidden-sm"><?=$description?></p>
									<div class="stick-top-right small-padding text-default-light text-sm"><?=$row['created_date']?></div>

									<?php
									if ($is_admin) {
										if (!$row['status']){
									?>
											<div class="stick-bottom-right small-padding" href="<?=base_url('order/approve')?>">
												<button type="button" class="btn ink-reaction btn-floating-action btn-default"
													 onclick="location.href='<?=base_url('order/delete/'.$row['id'])?>';" 
												>&nbsp;<i class="md md-clear"></i></button> &nbsp;
												<button type="button" class="btn ink-reaction btn-floating-action btn-primary"
													 onclick="location.href='<?=base_url('order/approve/'.$row['id'])?>';" 
												>&nbsp;<i class="md md-check"></i></button>
											</div>
									<?php
										}
									}
									else{?>
										<div class="stick-bottom-right small-padding"><span class="md <?=$icon?>"></span></div>
									<?php
									}?>
								</a>
							<?php
							}

							//If empty
							if ($total == 0){
								echo'Tidak ada data';
							}
							?>
						</div><!--end .list-group -->
					</div></div><div class="nano-pane"><div class="nano-slider" style="height: 95px; transform: translate(0px, 0px);"></div></div></div><!--end .col -->
					<!-- END INBOX EMAIL LIST -->

				</div><!--end .row -->
			</div><!--end .col -->
		</div>


	</div>
</section>

<!-- BEGIN SIMPLE MODAL MARKUP -->
<div class="modal fade" id="simpleModal" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="simpleModalLabel">Yakin akan membatalkan suplai</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a href="#" data-method="DELETE" id="btn-delete"><button type="button" class="btn btn-danger">Hapus</button></a>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END SIMPLE MODAL MARKUP -->