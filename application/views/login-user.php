<div class="text-xl text-bold text-primary text-center">WAREHOUSING</div>
<div class="text-default tsext-center text-center">Login Pengguna</div>
<form class="form floating-label" action="<?=base_url('login/login')?>" accept-charset="utf-8" method="post">
	
	<div class="form-group">
		<label for="username">Username</label>
		<input type="text" class="form-control" id="username" name="username" required="true">
	</div>
	<div class="form-group">
		<label for="password">Password</label>
		<input type="password" class="form-control" id="password" name="password" required="true">
		<!--<p class="help-block"><a href="#">Lupa password?</a></p>-->
	</div>
	<?php
	//Alert and validation
	if(isset($validation)){
		echo'
		<div class="form-group alert alert-warning" role="alert">
			'.$validation.'
		</div>';
	}
	else
		echo"<br/>";
	?>
	
	<div class="row">
		<div class="col-xs-6 text-left">
			<div class="checkbox checkbox-inline checkbox-styled">
				<label><input type="checkbox"> <span>Ingatkan saya</span></label>
			</div>
		</div><!--end .col -->
		<div class="col-xs-6 text-right">
			<button class="btn btn-primary btn-raised" type="submit">Login</button>
		</div><!--end .col -->
		<!--<div class="col-lg-12 text-center">
			<br>
			<a href="<?=base_url('login/admin')?>" class="text-info">Login sebagai administrator</a>
		</div>-->
	</div><!--end .row -->
</form>