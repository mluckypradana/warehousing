<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Material Admin - 404 page</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-default/bootstrap.css?1422792965" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-default/materialadmin.css?1425466319" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-default/font-awesome.min.css?1422529194" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/theme-default/material-design-iconic-font.min.css?1421434286" />
		<!-- END STYLESHEETS -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-default/libs/toastr/toastr.css">
	
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="assets/js/libs/utils/html5shiv.js?1403934957"></script>
		<script type="text/javascript" src="assets/js/libs/utils/respond.min.js?1403934956"></script>
		<![endif]-->
	</head>
	<body class="meheader-fixed ">
		<!-- BEGIN CONTENT-->
		<div id="content">

			<!-- BEGIN 404 MESSAGE -->
			<section>
				<div class="section-body contain-lg">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h1><span class="text-xxxl text-light">404 <i class="fa fa-search-minus text-primary"></i></span></h1>
							<h2 class="text-light">Halaman tidak tersedia</h2>
							<br/>
							<a href="<?php echo base_url();?>"><button type="button" class="btn ink-reaction btn-raised btn-primary">Ke halaman utama</button></a>
						</div><!--end .col -->
					</div><!--end .row -->
				</div><!--end .section-body -->	
			</section>
			<!-- END 404 MESSAGE -->

		</div><!--end #content-->
		<!-- END CONTENT -->

		<!-- BEGIN JAVASCRIPT -->
		<script src="assets/js/libs/jquery/jquery-1.11.2.min.js"></script>
		<script src="assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
		<script src="assets/js/libs/bootstrap/bootstrap.min.js"></script>
		<script src="assets/js/libs/spin.js/spin.min.js"></script>
		<script src="assets/js/libs/autosize/jquery.autosize.min.js"></script>
		<script src="assets/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
		<script src="assets/js/core/source/App.js"></script>
		<script src="assets/js/core/source/AppNavigation.js"></script>
		<script src="assets/js/core/source/AppOffcanvas.js"></script>
		<script src="assets/js/core/source/AppCard.js"></script>
		<script src="assets/js/core/source/AppForm.js"></script>
		<script src="assets/js/core/source/AppNavSearch.js"></script>
		<script src="assets/js/core/source/AppVendor.js"></script>
		<script src="assets/js/core/demo/Demo.js"></script>
		<!-- END JAVASCRIPT -->


<script src="<?php echo base_url();?>assets/js/libs/toastr/toastr.js"></script>
			<script src="<?php echo base_url();?>assets/js/core/demo/DemoUIMessages.js"></script>

			<script>
				toastr.options.hideDuration = 0;
				toastr.clear();
				
				toastr.options.closeButton = false;
				toastr.options.progressBar = false;
				toastr.options.debug = false;
				toastr.options.positionClass = 'toast-bottom-left';
				toastr.options.showDuration = 333;
				toastr.options.hideDuration = 333;
				toastr.options.timeOut = 4000;
				toastr.options.extendedTimeOut = 4000;
				toastr.options.showEasing = 'swing';
				toastr.options.hideEasing = 'swing';
				toastr.options.showMethod = 'slideDown';
				toastr.options.hideMethod = 'slideUp';
				
				toastr.options.timeOut = 0;
				toastr.options.positionClass = 'toast-bottom-center';
				toastr.warning('<?php echo	$error;?>', '');
			</script>
	</body>
</html>
