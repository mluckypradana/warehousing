<?php
class Lib_template {
	protected $_ci;
	function __construct() {
		$this->_ci=&get_instance();
	}
	function display($content,$data = null) {
		$data['_content'] = $this->_ci->load->view($content,$data,true);
		// $data['_header'] = $this->_ci->load->view('header',$data,true);
		// $data['_top_menu'] = $this->_ci->load->view('menu',$data,true);
		$this->_ci->load->view('admin',$data);
	}

	function display_template($template, $content, $data = null) {
		$data['_content'] = $this->_ci->load->view($content, $data, true);
		// $data['_header'] = $this->_ci->load->view('header',$data,true);
		// $data['_top_menu'] = $this->_ci->load->view('menu',$data,true);
		$this->_ci->load->view($template, $data);
	}
}
?>