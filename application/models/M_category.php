<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_category extends Generic_dao{
	public function table_name() {
		return Tables::$category;
	}

	public function __construct() {
		parent::__construct();
	}

	public function field_map() {
		return array(
			'id' => 'id',
			'name' => 'name'
		);
	}
}
?>