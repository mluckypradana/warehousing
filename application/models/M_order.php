<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_order extends Generic_dao{
	public function table_name() {
		return Tables::$orders;
	}

	public function __construct() {
		parent::__construct();
	}

	public function field_map() {
		return array(
			'id' => 'id',
			'order_number' => 'order_number',
			'status' => 'status',
			'user_id' => 'user_id',
			'item_id' => 'item_id',
			'approved_user_id' => 'approved_user_id',
			'total' => 'total',
			'created_date' => 'created_date',
			'updated_date' => 'updated_date'
		);
	}

	public function fetch_list($id = NULL) {
		$where = $id!=NULL ? " WHERE user_id = ".$id:"";
		return $this->ci->db->query("SELECT * FROM order_list".$where." ORDER BY created_date DESC")->result_array();
	}

	public function fetch_approved_list($id = NULL) {
		$where = $id!=NULL ? " AND user_id = ".$id:"";
		return $this->ci->db->query("SELECT * FROM order_list WHERE status = 1".$where."  ORDER BY created_date DESC")->result_array();
	}
	public function fetch_unapproved_list($id = NULL) {
		$where = $id!=NULL ? " AND user_id = ".$id:"";
		return $this->ci->db->query("SELECT * FROM order_list WHERE status != 1".$where." ORDER BY created_date DESC")->result_array();
	}
}
?>