<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_item extends Generic_dao{
	public function table_name() {
		return Tables::$item;
	}

	public function __construct() {
		parent::__construct();
	}

	public function field_map() {
		return array(
			'id'           => 'id',
			'name'          => 'name',
			'description'          => 'description',
			'total'          => 'total',
			'unit_name'          => 'unit_name',
			'price'          => 'price',
			'category_id'          => 'category_id',
			'created_date'          => 'created_date',
		);
	}

	public function fetch_list(){
			return $this->ci->db->query("SELECT * FROM item_list ORDER BY created_date DESC")->result_array();
	}


	public function add_stock($total, $id) {
		$result = $this->by_id(array('id'=>$id));
		$new_total = $result->total+$total;
		$this->update(array('total'=>$new_total), array('id'=>$id));
	}

	public function remove_stock($total, $id) {
		$this->add_stock($total*-1, $id);
	}
}
?>