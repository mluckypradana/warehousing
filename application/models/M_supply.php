<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_supply extends Generic_dao{
	public function table_name() {
		return Tables::$supply;
	}

	public function __construct() {
		parent::__construct();
	}

	public function field_map() {
		return array(
			'id' => 'id',
			'total' => 'total',
			'user_id' => 'user_id',
			'item_id' => 'item_id',
			'created_date' => 'created_date'
		);
	}

	public function fetch_list() {
		return $this->ci->db->query("SELECT * FROM supply_list ORDER BY created_date DESC")->result_array();
	}
}
?>