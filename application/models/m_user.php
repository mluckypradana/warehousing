<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_user extends Generic_dao{
	public $table = 'user';

	public function table_name() {
        return Tables::$user;
    }

    public function field_map() {
        return array(
            'id' => 'id',
            'name' => 'name',
            'email' => 'email',
            'phone' => 'phone',
            'address' => 'address',
            'username' => 'username',
            'password' => 'password',
            'role' => 'role',
            'created_date' => 'created_date',
            'allow_supply' => 'allow_supply'
        );
    }

    public function __construct() {
        parent::__construct();
    }

	public function login($data) {
		$data['password'] = md5($data['password']);
		return $this->ci->db->get_where($this->table, $data, 0, 0);
	}
}
?>