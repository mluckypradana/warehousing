<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends AdminController {
	public $dbs;
	public $object_label = 'pengguna';
	public $default_password = '12345678';

	public function __construct() {
		parent::__construct();
        init_generic_dao();
		$this->load->model('m_user');
		$this->load->library('lib_template');
		$this->dbs = $this->m_user;
	}

	public function index() {
		$data['nav'] = $this->nav;
		$data['object_label'] = $this->object_label;
		$data['validation'] = '';
		$data['add_link'] = base_url($this->nav.'/tambah');
		$data['list'] = $this->to_array($this->dbs->fetch());

		//Display layout based on total data
		$total = count($data['list']);
		$this->lib_template->display($total > 0? $this->nav: 'no-data', $data);
	}

	//Show add form
	public function tambah() {
		if ($this->method == 'POST') $this->add(false);
		else $this->show_form(NULL, false);
	}

	//Show update form / execute update
	public function edit() {
		if ($this->method == 'POST') $this->add(true);
		else $this->show_form(NULL, true);
	}

	//Show add / edit form
	function show_form($data, $is_edit) {
		$this->load->helper('form');
		$data['nav'] = $this->nav;
		$data['is_edit'] = $is_edit;

		if (!$is_edit) {
			//Define default value
			$data['data'] = $this->get_post();
			$data['form_action'] = $this->action_add;

			$data['label_submit'] = $this->label_add;
			$label_title    = $this->label_add_title;
			$label_subtitle = $this->label_add_subtitle;
		}
		else {
			$id     = $this->uri->segment(3);
			$result = $this->dbs->by_id(array('id'=>$id));//->result_array();
			if (count($result) == 0) {
				redirect(base_url('not_found'));
				return;
			}
			$data['data'] = $this->to_array($result);
			$data['form_action'] = base_url($this->nav.'/'.$this->action_update);
			$data['label_submit'] = $this->label_update;
			$label_title    = $this->label_update_title;
			$label_subtitle = $this->label_update_subtitle;
		}

		//Set title and subtitlelabel
		$data['label_title'] = str_replace('%s', $this->object_label,$label_title);
		$data['label_subtitle'] = str_replace('%s', $this->object_label,$label_subtitle);

		$this->lib_template->display($this->nav.'-form', $data);
	}

	//Insert data to database, can be update
	function add($is_edit) {
		$data['empty'] = '';
		if ($this->data_valid()) {
			$param = $this->get_post();
			if (!$is_edit) {
				$param['password'] = md5($param['password']);
				// echo json_encode($param);
				$this->dbs->insert($param);
				$flash = $this->label_added;
			}
			else {
				$this->dbs->update($param, array('id'=>$param['id']));
				$flash = $this->label_updated;
			}
			$this->session->set_flashdata('success', $flash);
			redirect(base_url($this->nav),'refresh');
		}
		else {
			$this->show_form($data, $is_edit);
		}
	}


	//Remove single data
	function change_password() {
		$param['id'] = $this->input->post('id');
		$param['password'] = $this->input->post('password');
		$param['confirm_password'] = $this->input->post('confirm_password');
		
		if($param['password'] == $param['confirm_password']){
			unset($param['confirm_password']);
			$param['password'] = md5($param['password']);
			$this->dbs->update($param, array('id'=>$param['id']));
			$this->session->set_flashdata('success', 'Berhasil mengubah password');
		}
		else
			$this->session->set_flashdata('warning', 'Konfirmasi password tidak cocok');
		redirect(base_url('dashboard'),'refresh');
	}


	//Remove single data
	function delete() {
		$id = $this->uri->segment(3);
		$this->dbs->delete(array('id'=> $id));
		$this->session->set_flashdata('success', $this->label_deleted);
		redirect(base_url($this->nav),'refresh');
	}

	//Get data from post
	public function get_post() {
		$post = array(
			'id'           => $this->input->post('id'),
			'name'           => $this->input->post('name'),
			'email'           => $this->input->post('email'),
			'phone'           => $this->input->post('phone'),
			'address'           => $this->input->post('address'),
			'username'           => $this->input->post('username'),
			'password'           => $this->input->post('password'),
			'role'           => $this->input->post('role'),
			'allow_supply'           => $this->input->post('allow_supply')
		);

		//Set default password
		if(!isset($post['password'])) {
			$post['password'] = $this->default_password;
		}

		//Translate checkboxes
		if(isset($post['role']))
			$post['role'] = 1;
		else
			unset($post['role']);
		
		
		if(isset($post['allow_supply']))
			$post['allow_supply'] = 1;
		else
			unset($post['allow_supply']);
		return $post;
	}

	//Validate post data with form - validation
	public function data_valid() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name','Nama','required', $this->required_pattern);
		$this->form_validation->set_rules('email','Email','required', $this->required_pattern);
		$this->form_validation->set_rules('username','Username','required', $this->required_pattern);
		return $this->form_validation->run();
	}
}