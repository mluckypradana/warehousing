<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends AdminController {
	public $data;
	
	function __construct() {
		parent::__construct();
		init_generic_dao();
		$this->load->library('lib_template');
		$this->load->model(array('m_user','m_item','m_category'));
		$this->load->library('form_validation');
		$this->data['nav'] = $this->uri->segment(1);


	}

	function index() {
		$data['data'] = $this->get_post();
		$data['nav'] = $this->uri->segment(1);
		$session = $this->session->get_userdata(); 
		$data['session_name'] = $session['name'];
		$data['session_id'] = $session['id'];
		$data['session_role'] = $session['role'];
		$role_admin = 1;
		$data['is_admin'] = $data['session_role'] == $role_admin;
		$data['session_role_name'] = $data['is_admin']? 'Administrator': 'Pengguna';

		//Get total data
		$data['user_total'] = count($this->to_array($this->m_user->fetch()));
		$data['item_total'] = count($this->to_array($this->m_item->fetch()));
		$data['category_total'] = count($this->to_array($this->m_category->fetch()));

		//Get user data to compare password
		$user = $this->to_array($this->m_user->by_id(array('id'=>$data['session_id'])));
		$default_password = md5('12345678');

		//If user need to change password
		$data['need_change_password'] = false;
		if($user['password'] == $default_password) {
			$data['need_change_password'] = true;
		}
	
		$this->lib_template->display('dashboard', $data);
	}

		public function get_post() {
		return array(
			'password'=> $this->input->post('password'),
			'confirm_password'=> $this->input->post('confirm_password')
		);
	}
}