<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
        init_generic_dao();
		$this->load->library('lib_template');
		$this->load->model('m_user');

		$user = $this->session->get_userdata();
		if (isset($user['id'])) redirect(base_url('dashboard'),'refresh');
	}

	public function index() {
		$this->user();
	}

	//public function admin() {
	//	$this->lib_template->display_template('login','login-admin');
	//}
	
	public function user($data = NULL) {
		$this->lib_template->display_template('login','login-user', $data);
	}

	//Login from page
	public function login() {
		//Check login
		$param = $this->get_post_data();
		$query = $this->m_user->login($param);
		$total = $query->num_rows();

		//If success redirect to admin page
		if ($total) {
			$user = array(
				'id'         => $query->row()->id,
				'name'       => $query->row()->name,
				'username'   => $query->row()->username,
				'role'   => $query->row()->role,
				'allow_supply'   => $query->row()->allow_supply
			);
			$this->session->set_userdata($user);
			redirect(base_url('dashboard'),'refresh');
		}

		//If failed show validation error
		else {
			$data['validation'] = 'Username atau password tidak cocok';
			$this->user($data);
		}
	}

	public function get_post_data() {
		return array(
			'username'=> $this->input->post('username'),
			'password'=> $this->input->post('password')
		);
	}

}
