<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends AdminController {
	public $dbs;
	public $object_label = 'pemesanan';

	public function __construct() {
		parent::__construct();
        init_generic_dao();
		$this->load->model(array('m_order','m_item'));
		$this->load->library('lib_template');
		$this->dbs = $this->m_order;
	}

	public function index() {
		$this->all();
	}

	function get_basic_page_data(){
		//Session
		$session = $this->session->get_userdata(); 
		$session_name = $session['name'];
		$session_id = $session['id'];
		$session_role = $session['role'];
		$role_admin = 1;
		$is_admin = $session_role == $role_admin;
		$session_role_name = $is_admin? 'Administrator': 'Pengguna';
		$id = $is_admin ? NULL : $session_id;

		$data['subnav'] = $this->uri->segment(2);
		$data['nav'] = $this->nav;
		$data['object_label'] = $this->object_label;
		$data['validation'] = '';
		$data['add_link'] = base_url($this->nav.'/tambah');
		$data['list'] = $this->to_array($this->dbs->fetch_list($id));
		$data['approved_list'] = $this->to_array($this->dbs->fetch_approved_list($id));
		$data['unapproved_list'] = $this->to_array($this->dbs->fetch_unapproved_list($id));

		//Display layout based on total data
		$total = count($data['list']);
		$data['all_total'] = $total;
		$data['approved_total'] = count($data['approved_list']);
		$data['unapproved_total'] = count($data['unapproved_list']);
		return $data;
	}

	public function all() {
		$data = $this->get_basic_page_data();
		$data['total'] = $data['all_total'];
		$this->lib_template->display($this->nav, $data);
	}

	public function approved() {
		$data = $this->get_basic_page_data();
		$data['list'] = $data['approved_list'];
		$data['total'] = $data['approved_total'];
		$this->lib_template->display($this->nav, $data);
	}


	public function unapproved() {
		$data = $this->get_basic_page_data();
		$data['list'] = $data['unapproved_list'];
		$data['total'] = $data['unapproved_total'];


		$this->lib_template->display($this->nav, $data);
	}

	//Show add form
	public function tambah() {
		if ($this->method == 'POST') $this->add(false);
		else $this->show_form(NULL, false);
	}

	//Show update form / execute update
	public function edit() {
		if ($this->method == 'POST') $this->add(true);
		else $this->show_form(NULL, true);
	}

	//Show add / edit form
	function show_form($data, $is_edit) {
		$this->load->helper('form');
		$data['nav'] = $this->nav;

		if (!$is_edit) {
			//Define default value
			$data['data'] = $this->get_post();
			$data['form_action'] = $this->action_add;

			$data['label_submit'] = $this->label_add;
			$label_title    = $this->label_add_title;
			$label_subtitle = $this->label_add_subtitle;
		}
		else {
			$id     = $this->uri->segment(3);
			$result = $this->dbs->by_id(array('id'=>$id));//->result_array();
			if (count($result) == 0) {
				redirect(base_url('not_found'));
				return;
			}
			$data['data'] = $this->to_array($result);
			$data['form_action'] = base_url($this->nav.'/'.$this->action_update);
			$data['label_submit'] = $this->label_update;
			$label_title    = $this->label_update_title;
			$label_subtitle = $this->label_update_subtitle;
		}

		//Set title and subtitlelabel
		$data['label_title'] = str_replace('%s', $this->object_label,$label_title);
		$data['label_subtitle'] = str_replace('%s', $this->object_label,$label_subtitle);
		$data['item'] = $this->to_array($this->m_item->fetch());

		$this->lib_template->display($this->nav.'-form', $data);
	}

	//Insert data to database, can be update
	function add($is_edit) {
		$data['empty'] = '';
		if ($this->data_valid()) {
			$param = $this->get_post();

			//If insert
			if (!$is_edit) {
				//Generate params
				$param['order_number'] = 'WH'.date('Ymd-Hi');//audit trail
				unset($param['status']);
				$item = $this->m_item->by_id(array('id'=>$param['item_id']));

				//If quantity met
				if($param['total'] <= $item->total){
					$this->dbs->insert($param);
					$flash = 'Berhasil melakukan permintaan, harap tunggu persetujuan adminstrator';//$this->label_added;
					$this->session->set_flashdata('success', $flash);
					
				}

				//Else, show warning
				else{
					$flash = 'Stok tidak memenuhi jumlah yang dipinjam';
					$this->session->set_flashdata('warning', $flash);
					$this->show_form($data, $is_edit);
					return;
				}
			}

			//If update
			else {
				$this->dbs->update($param, array('id'=>$param['id']));
				$flash = $this->label_updated;
				$this->session->set_flashdata('success', $flash);
			}
			redirect(base_url($this->nav),'refresh');
		}
		else 
			$this->show_form($data, $is_edit);
	}



	//Remove single data
	function delete() {
		$id = $this->uri->segment(3);
		$this->dbs->delete(array('id'=> $id));
		$this->session->set_flashdata('success', $this->label_deleted);
		redirect(base_url($this->nav),'refresh');
	}

	//Approve
	public function approve() {
		//Generate params
		$id = $this->uri->segment(3);
		$param = $this->to_array($this->dbs->by_id(array('id'=>$id)));
		$item = $this->m_item->by_id(array('id'=>$param['item_id']));

		//If quantity met
		if($param['total'] <= $item->total){
			//Remove stock
			$this->m_item->remove_stock($param['total'],$param['item_id']);

			//Set approved and status
			$param['status'] = 1;
			$session = $this->session->get_userdata(); 
			$session_name = $session['name'];
			$session_id = $session['id'];
			$param['approved_user_id'] = $session_id;

			//Update
			$this->dbs->update($param, array('id'=>$param['id']));
			$flash = 'Berhasil menyetujui permintaan';//$this->label_added;
			$this->session->set_flashdata('success', $flash);
		}
		else {
			$flash = 'Stok tidak memenuhi jumlah yang dipinjam';
			$this->session->set_flashdata('warning', $flash);
		}
		redirect(base_url($this->nav),'refresh');
	}

	//Get data from post
	public function get_post() {
		$post = array(
			'id'           => $this->input->post('id'),
			'order_number'         => $this->input->post('order_number'),
			'status'         => $this->input->post('status'),
			'user_id'         => $this->input->post('user_id'),
			'approved_user_id'         => $this->input->post('approved_user_id'),
			'item_id'         => $this->input->post('item_id'),
			'total'         => $this->input->post('total')
		);
		return $post;
	}

	//Validate post data with form - validation
	public function data_valid() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('item_id','Barang','required', $this->required_pattern);
		$this->form_validation->set_rules('total','Jumlah yang dipinjam','required', $this->required_pattern);
		return $this->form_validation->run();
	}
}