<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item extends AdminController {
	public $dbs;
	public $object_label = 'barang';

	public function __construct() {
		parent::__construct();
        init_generic_dao();
		$this->load->model(array('m_item','m_category'));
		$this->load->library('lib_template');
		$this->dbs = $this->m_item;
	}

	public function index() {
		$data['nav'] = $this->nav;
		$data['object_label'] = $this->object_label;
		$data['validation'] = '';
		$data['add_link'] = base_url($this->nav.'/tambah');
		$data['list'] = $this->to_array($this->dbs->fetch_list());
		
		//Display layout based on total data
		$total = count($data['list']);
		$this->lib_template->display($total > 0? $this->nav: 'no-data', $data);
	}

	//Show add form
	public function tambah() {
		if ($this->method == 'POST') $this->add(false);
		else $this->show_form(NULL, false);
	}

	//Show update form / execute update
	public function edit() {
		if ($this->method == 'POST') $this->add(true);
		else $this->show_form(NULL, true);
	}

	//Show add / edit form
	function show_form($data, $is_edit) {
		$this->load->helper('form');
		$data['nav'] = $this->nav;

		if (!$is_edit) {
			//Define default value
			$data['data'] = $this->get_post();
			$data['form_action'] = $this->action_add;

			$data['label_submit'] = $this->label_add;
			$label_title    = $this->label_add_title;
			$label_subtitle = $this->label_add_subtitle;
		}
		else {
			$id     = $this->uri->segment(3);
			$result = $this->dbs->by_id(array('id'=>$id));//->result_array();
			if (count($result) == 0) {
				redirect(base_url('not_found'));
				return;
			}
			$data['data'] = $this->to_array($result);
			$data['form_action'] = base_url($this->nav.'/'.$this->action_update);
			$data['label_submit'] = $this->label_update;
			$label_title    = $this->label_update_title;
			$label_subtitle = $this->label_update_subtitle;
		}

		//Set title and subtitlelabel
		$data['label_title'] = str_replace('%s', $this->object_label,$label_title);
		$data['label_subtitle'] = str_replace('%s', $this->object_label,$label_subtitle);
		$data['category'] = $this->to_array($this->m_category->fetch());

		$this->lib_template->display($this->nav.'-form', $data);
	}

	//Insert data to database, can be update
	function add($is_edit) {
		$data['empty'] = '';
		if ($this->data_valid()) {
			$param = $this->get_post();
			if (!$is_edit) {
				$this->dbs->insert($param);
				$flash = $this->label_added;
			}
			else {
				$this->dbs->update($param, array('id'=>$param['id']));
				$flash = $this->label_updated;
			}
			$this->session->set_flashdata('success', $flash);
			redirect(base_url($this->nav),'refresh');
		}
		else {
			$this->show_form($data, $is_edit);
		}
	}


	//Remove single data
	function delete() {
		$id = $this->uri->segment(3);
		$this->dbs->delete(array('id'=> $id));
		$this->session->set_flashdata('success', $this->label_deleted);
		redirect(base_url($this->nav),'refresh');
	}

	//Remove single data
	function detail() {
		$id = $this->uri->segment(3);
		$this->dbs->fetch_record(array('id'=> $id));
		$this->lib_template->display($this->nav.'-detail', $data);
	}

	//Get data from post
	public function get_post() {
		$post = array(
			'id'           => $this->input->post('id'),
			'name'          => $this->input->post('name'),
			'description'          => $this->input->post('description'),
			'total'          => $this->input->post('total'),
			'unit_name'          => $this->input->post('unit_name'),
			'price'          => $this->input->post('price'),
			'category_id'          => $this->input->post('category_id'),
		);
		return $post;
	}

	//Validate post data with form - validation
	public function data_valid() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name','Nama','required', $this->required_pattern);
		$this->form_validation->set_rules('total','Stok','required', $this->required_pattern);
		$this->form_validation->set_rules('unit_name','Satuan','required', $this->required_pattern);
		$this->form_validation->set_rules('price','Price','required', $this->required_pattern);
		$this->form_validation->set_rules('total','Stok','required', $this->required_pattern);
		$this->form_validation->set_rules('category_id','Kategori','required', $this->required_pattern);
		return $this->form_validation->run();
	}
}