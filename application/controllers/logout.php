<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->session->unset_userdata(array('id','nama','username','madrasah'));
		redirect(base_url('login'),'refresh');
	}
}
