<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/libraries/config.html
 */
class CI_Model {

	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		log_message('info', 'Model Class Initialized');
	}

	// --------------------------------------------------------------------

	/**
	 * __get magic
	 *
	 * Allows models to access CI's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string	$key
	 */
	public function __get($key)
	{
		// Debugging note:
		//	If you're here because you're getting an error message
		//	saying 'Undefined Property: system/core/Model.php', it's
		//	most likely a typo in your model code.
		return get_instance()->$key;
	}

	public function get_all(){
		$query = $this->db->get($this->table); 
    	return $query->result_array();
	}

	public function get($id) {
		$query = $this->db->get_where($this->table, array('id' => $id), 0, 0);
    	return $query;
	}

	public function insert($data){
        return $this->db->insert($this->table, $data);
    }
 
    public function update($data){
    	$where = array('id' => $data['id']);
        return $this->db->update($this->table, $data, $where);
    }
     
    public function delete($where){
        return $this->db->delete($this->table, $where);
    }

    //Get all data for API (Return more than real data)
	public function get_all_api(){
		$query = $this->db->get($this->table);
    	$ret = $query->result_array();

    	$result['message'] = '';
    	$result['data'] = $ret;
   		return $result;
	}

    //Get method for API (Return more than real data)
	public function get_api($id){
		$query = $this->db->get_where($this->table, array('id' => $id), 0, 0);
    	$ret = $query->result_object()[0];

    	$result['message'] = '';
    	$result['data'] = $ret;
   		return $result;
	}

	//Insert method for API (Return more than real data)
	public function insert_api($data){
        $this->insert($data);
    	$id = $this->db->insert_id();

    	$result['message'] = '';
    	$result['data'] = $this->get($id)->result_object()[0];
	    return $result;
    }
    
    //Insert method for API (Return more than real data)
	public function update_api($data){
        $this->update($data);
    	$id = $data['id'];
		if($id==NULL){
			$result['message'] = 'id can\'t be null';
			return $result;	
		}

    	$result['message'] = '';
    	$result['data'] = $this->get($id)->result_object()[0];
	    return $result;
    }
    
    public function delete_api($id){
    	$result['message'] = '';
		$this->delete(array('id'=>$id));
		return $result;
	}
}
