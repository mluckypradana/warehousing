<?php
/**
* CodeIgniter
*
* An open source application development framework for PHP
*
* This content is released under the MIT License (MIT)
*
* Copyright (c) 2014 - 2017, British Columbia Institute of Technology
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
* @package	CodeIgniter
* @author	EllisLab Dev Team
* @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
* @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
* @license	http://opensource.org/licenses/MIT	MIT License
* @link	https://codeigniter.com
* @since	Version 1.0.0
* @filesource
*/
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Application Controller Class
*
* This class object is the super class that every library in
* CodeIgniter will be assigned to.
*
* @package		CodeIgniter
* @subpackage	Libraries
* @category	Libraries
* @author		EllisLab Dev Team
* @link		https://codeigniter.com/user_guide/general/controllers.html
*/
class CI_Controller {
	private static $default_error_message = "Terjadi kesalahan harap coba lagi.";

	/**
	* Reference to the CI singleton
	*
	* @var	object
	*/
	private static $instance;

	/**
	* Class constructor
	*
	* @return	void
	*/
	public function __construct() {
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class) {
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');
		$this->load->initialize();
		log_message('info', 'Controller Class Initialized');
	}

	// --------------------------------------------------------------------

	/**
	* Get the CI singleton
	*
	* @static
	* @return	object
	*/
	public static function & get_instance() {
		return self::$instance;
	}

	public function json($code, $message, $param) {
		$result['message'] = $message;
		if($param!=null)
			$result['data'] = $param;
		header('HTTP / 1.1 '.$code, true, $code);
		echo json_encode($result);
	}
}

class AdminController extends CI_Controller {
	public $user;
	public $is_api = false;
	public $method;
	public $nav = "";
	
	//Form validation message pattern and label
	public $action_add = "tambah";
	public $action_update = "edit/";
	public $required_pattern = array('required'=> '%s wajib diisi');
	
	public $label_add = 'Simpan';
	public $label_update = 'Ubah';
	public $label_add_title = 'Tambah %s';
	public $label_update_title = 'Ubah %s';
	public $label_add_subtitle = 'Silakan isi formulir dibawah ini untuk menambah data %s.';
	public $label_update_subtitle = 'Silakan isi formulir dibawah ini untuk mengubah data %s.';
	public $label_added = 'Berhasil menambah data';
	public $label_updated = 'Berhasil mengubah data';
	public $label_deleted = 'Berhasil menghapus data';
	
	public function __construct() {
		parent::__construct();
		
		//Get nav url
		$this->nav = $this->uri->segment(1);
		
		//Check if this request is for API or regular access
		$this->method = $_SERVER['REQUEST_METHOD'];
		$token = $this->input->post('token',true);
		if(isset($token)){
			$this->is_api = true;
		}
		else{
		
			//Validate admin session
			$this->user = $this->session->get_userdata();
			if (!isset($this->user['id'])) redirect(base_url(),'refresh');
			else $data['session'] = $this->session->get_userdata();
		}
		
	}

	public function load_view($viewname, $data) {
		$data['nav'] = $this->nav;
		$this->load->view('admin-header',$data);
		$this->load->view($viewname, $data);
		$this->load->view('admin-footer', $data);
	}
	
	public function convert_date($input){
		$array = explode("/",$input);
		return $array[2]."-".$array[0]."-".$array[1];
	}

    function to_array($data) {
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = $this->to_array($value);
        }
        return $result;
    }
    return $data;
}
}
