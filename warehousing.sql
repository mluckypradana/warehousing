-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11 Jun 2017 pada 06.48
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `warehousing`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Perlengkapan Sekolah'),
(2, 'Perlengkapan Kantor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `description` text,
  `total` int(11) DEFAULT NULL,
  `unit_name` varchar(32) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `item`
--

INSERT INTO `item` (`id`, `name`, `description`, `total`, `unit_name`, `price`, `category_id`, `created_date`) VALUES
(1, 'Buku Tulis', NULL, 10, 'Lusin', 1000, 1, '2017-06-09 07:14:55'),
(2, 'Buku Gambar', NULL, 24, 'lusin', 1500, 1, '2017-06-09 07:15:52');

-- --------------------------------------------------------

--
-- Stand-in structure for view `item_list`
--
CREATE TABLE `item_list` (
`id` int(11)
,`name` varchar(32)
,`description` text
,`total` int(11)
,`unit_name` varchar(32)
,`price` int(11)
,`category_id` int(11)
,`created_date` timestamp
,`category_name` varchar(32)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_number` varchar(32) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `approved_user_id` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `order_number`, `status`, `user_id`, `item_id`, `approved_user_id`, `total`, `created_date`, `updated_date`) VALUES
(4, 'WH20170609-1333', 1, 2, 2, 1, 2, '2017-06-09 11:33:57', '2017-06-09 11:33:57'),
(5, 'WH20170609-1344', 1, 3, 1, 1, 1, '2017-06-09 11:44:29', '2017-06-09 11:44:29'),
(6, 'WH20170609-1345', 1, 2, 1, 1, 3, '2017-06-09 11:45:11', '2017-06-09 11:45:11'),
(7, 'WH20170611-0612', 1, 4, 1, 1, 20, '2017-06-11 04:12:51', '2017-06-11 04:12:51');

-- --------------------------------------------------------

--
-- Stand-in structure for view `order_list`
--
CREATE TABLE `order_list` (
`id` int(11)
,`order_number` varchar(32)
,`status` int(11)
,`user_id` int(11)
,`item_id` int(11)
,`approved_user_id` int(11)
,`total` int(11)
,`created_date` timestamp
,`updated_date` timestamp
,`item_name` varchar(32)
,`user_name` varchar(32)
,`approved_user_name` varchar(32)
,`unit_name` varchar(32)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `supply`
--

CREATE TABLE `supply` (
  `id` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supply`
--

INSERT INTO `supply` (`id`, `total`, `user_id`, `item_id`, `created_date`) VALUES
(1, 5, 1, 1, '2017-06-11 03:54:46'),
(2, 4, 1, 1, '2017-06-11 03:55:02'),
(3, 5, 1, 1, '2017-06-11 04:08:56');

-- --------------------------------------------------------

--
-- Stand-in structure for view `supply_list`
--
CREATE TABLE `supply_list` (
`id` int(11)
,`total` int(11)
,`user_id` int(11)
,`item_id` int(11)
,`created_date` timestamp
,`item_name` varchar(32)
,`user_name` varchar(32)
,`unit_name` varchar(32)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `address` text,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `role` int(11) DEFAULT '0',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `allow_supply` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `phone`, `address`, `username`, `password`, `role`, `created_date`, `allow_supply`) VALUES
(1, 'John Doe', 'johndoe@gmail.com', '085956762979', 'Cidahu', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '2017-06-09 07:11:45', 0),
(2, 'Muhammad Lucky Pradana', 'mluckypradana@gmail.com', '85956762979', 'Pesona Fajar Asri A5 Cidahu', 'mluckypradana', '491dc4bd0f1d62dd1df76b2494eeeb4e', 0, '2017-06-09 13:33:36', 1),
(3, 'Septono', 'septono@yahoo.com', '018234192347', NULL, 'septono', '25d55ad283aa400af464c76d713c07ad', 0, '2017-06-09 10:51:47', 0),
(4, 'Kusnadi', 'kusnadi@gmail.com', '19237143832', 'CIdahu', 'kusnadi', '87509b7f1182014f1948aa8e699848d1', 0, '2017-06-11 04:11:01', 1);

-- --------------------------------------------------------

--
-- Struktur untuk view `item_list`
--
DROP TABLE IF EXISTS `item_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `item_list`  AS  select `i`.`id` AS `id`,`i`.`name` AS `name`,`i`.`description` AS `description`,`i`.`total` AS `total`,`i`.`unit_name` AS `unit_name`,`i`.`price` AS `price`,`i`.`category_id` AS `category_id`,`i`.`created_date` AS `created_date`,`c`.`name` AS `category_name` from (`item` `i` left join `category` `c` on((`c`.`id` = `i`.`category_id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `order_list`
--
DROP TABLE IF EXISTS `order_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `order_list`  AS  select `o`.`id` AS `id`,`o`.`order_number` AS `order_number`,`o`.`status` AS `status`,`o`.`user_id` AS `user_id`,`o`.`item_id` AS `item_id`,`o`.`approved_user_id` AS `approved_user_id`,`o`.`total` AS `total`,`o`.`created_date` AS `created_date`,`o`.`updated_date` AS `updated_date`,coalesce(`i`.`name`,'-') AS `item_name`,coalesce(`u`.`name`,'-') AS `user_name`,coalesce(`a`.`name`,'-') AS `approved_user_name`,`i`.`unit_name` AS `unit_name` from (((`orders` `o` left join `user` `u` on((`o`.`user_id` = `u`.`id`))) left join `user` `a` on((`o`.`approved_user_id` = `a`.`id`))) left join `item` `i` on((`o`.`item_id` = `i`.`id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `supply_list`
--
DROP TABLE IF EXISTS `supply_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `supply_list`  AS  select `s`.`id` AS `id`,`s`.`total` AS `total`,`s`.`user_id` AS `user_id`,`s`.`item_id` AS `item_id`,`s`.`created_date` AS `created_date`,coalesce(`i`.`name`,'-') AS `item_name`,coalesce(`u`.`name`,'-') AS `user_name`,`i`.`unit_name` AS `unit_name` from ((`supply` `s` left join `item` `i` on((`s`.`item_id` = `i`.`id`))) left join `user` `u` on((`s`.`user_id` = `u`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supply`
--
ALTER TABLE `supply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `supply`
--
ALTER TABLE `supply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
